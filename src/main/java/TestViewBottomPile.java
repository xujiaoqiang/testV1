import java.util.Observer;
import java.util.Observable;

public class TestViewBottomPile implements Observer{

    public void update(Observable o, Object arg) {

        int size = Integer.parseInt(arg.toString());
//        System.out.println("Receive a message and ViewBottomPile changed!"+size);
        if(size < 6){
            System.out.println("Receive a message and ViewBottomPile changed!");
        }
        else
            System.out.println("Receive a message and ViewBottomPile not changed!");
    }
}
